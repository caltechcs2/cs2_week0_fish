CPPFLAGS=-g -O3 -Wall -Wextra -pedantic
LDLIBS=-lstdc++ -lm -lpthread -lopencv_core -lopencv_ml

all: fish

fish: fish.o

clean:
	rm -f *~ *.o fish

.PHONY: all clean

